var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model Pessoa
module.exports = mongoose.model('Pessoa', new Schema({ 
	nome: String, 
	cpf: String,
	datanascimento:{type: Date} 
	telefone: String, 
	celular: String, 
	email: String, 
	whatsapp: String, 
	endereco: String, 
	bairro: String,
	numero: String, 
	complemento: String, 
	cep: String, 
	uf: {type: mongoose.Schema.Types.ObjectId, ref: 'Estado'},
    cidade: {type: mongoose.Schema.Types.ObjectId, ref: 'Cidade'},
	endereco: String
}));